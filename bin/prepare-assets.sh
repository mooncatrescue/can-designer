#!/bin/bash
TINYAPIKEY="2zR51FsL4x1krVfg5b3Qq7qc9RTD1rrM"
INPUT_DIR="renders"
PROCESSING_DIR="processing"
OUTPUT_DIR="assets"

# NOTE: This script expects a single folder named 'renders' containing videos
#  of both Cans and VMs named as follows (for example Mint #0):
#     Can - 0.mp4
#     VM - vm_0.mp4

# Function for processing files through Tinify web service
tinify_image () {
  filename=$1
  tinify=$(curl -sSL -D - https://api.tinify.com/shrink --user api:$TINYAPIKEY --data-binary @"${PROCESSING_DIR}/${filename}" -o /dev/null | grep location | awk '{print $2 }')
  tinify=${tinify// }
  tinify=`echo -n "$tinify"| sed s/.$//`
  curl ${tinify} -o "${OUTPUT_DIR}/${filename}" --silent
}

# Create processing dir if it does not already exist
if [ ! -d "$PROCESSING_DIR" ]; then
    echo "Creating processing dir \"$PROCESSING_DIR\"..."
    mkdir $PROCESSING_DIR   
fi

# Create output dir if it does not already exist
if [ ! -d "$OUTPUT_DIR" ]; then
    echo "Creating output dir \"$OUTPUT_DIR\"..."
    mkdir $OUTPUT_DIR   
fi

# Loop through the expected Mint number range processing videos into stills
for i in {0..1}
do
  echo "Processing videos for Mint #$i"
  ffmpeg -ss 00:00:00 -i "${INPUT_DIR}/$i.mp4" -vframes 1 -q:v 2 -vf scale=505:-1 "${PROCESSING_DIR}/front_$i.jpg" -hide_banner -loglevel error
  tinify_image "front_$i.jpg"
  ffmpeg -ss 00:00:05 -i "${INPUT_DIR}/$i.mp4" -vframes 1 -q:v 2 -vf scale=505:-1 "${PROCESSING_DIR}/label_$i.jpg" -hide_banner -loglevel error
  tinify_image "label_$i.jpg"
  ffmpeg -ss 00:00:00 -i "${INPUT_DIR}/vm_$i.mp4" -vframes 1 -q:v 2 -vf scale=505:-1 "${PROCESSING_DIR}/vm_$i.jpg" -hide_banner -loglevel error
  tinify_image "vm_$i.jpg"

  # Create output sub dir if it does not already exist
  if [ ! -d "${OUTPUT_DIR}/$i" ]; then
    echo "Creating output dir \"$OUTPUT_DIR/$i\"..."
    mkdir "${OUTPUT_DIR}/$i"  
  fi 

  # Move files to desired locations and tidy up
  cp "${INPUT_DIR}/$i.mp4" "${OUTPUT_DIR}/$i/$i.mp4"
  mv "${OUTPUT_DIR}/front_$i.jpg" "${OUTPUT_DIR}/$i/front_$i.jpg"
  mv "${OUTPUT_DIR}/label_$i.jpg" "${OUTPUT_DIR}/$i/label_$i.jpg"
  cp "${INPUT_DIR}/vm_$i.mp4" "${OUTPUT_DIR}/$i/vm_$i.mp4"
  mv "${OUTPUT_DIR}/vm_$i.jpg" "${OUTPUT_DIR}/$i/vm_$i.jpg"
  rm "${PROCESSING_DIR}/front_$i.jpg"
  rm "${PROCESSING_DIR}/label_$i.jpg"
  rm "${PROCESSING_DIR}/vm_$i.jpg"
done

rm -r "${PROCESSING_DIR}"
echo "Processing Complete."