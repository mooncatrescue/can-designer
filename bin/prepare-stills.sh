#!/bin/bash
INPUT_DIR="renders"
OUTPUT_DIR="stills"

# NOTE: This script expects a single folder named 'renders' containing videos
#  of both Cans and VMs named as follows (for example Mint #0):
#     Can - 0.mp4
#     VM - vm_0.mp4

# Create output dir if it does not already exist
if [ ! -d "$OUTPUT_DIR" ]; then
    echo "Creating output dir \"$OUTPUT_DIR\"..."
    mkdir $OUTPUT_DIR   
fi

# Loop through the expected Mint number range processing videos into stills
for i in {0..83}
do
  echo "Processing videos for Mint #$i"
  ffmpeg -ss 00:00:00 -i "${INPUT_DIR}/$i.mp4" -vframes 1 -q:v 2 "${OUTPUT_DIR}/$i.jpg" -hide_banner -loglevel error
  ffmpeg -ss 00:00:00 -i "${INPUT_DIR}/vm_$i.mp4" -vframes 1 -q:v 2 "${OUTPUT_DIR}/vm_$i.jpg" -hide_banner -loglevel error
done

echo "Processing Complete."