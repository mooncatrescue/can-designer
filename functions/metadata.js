const fs = require('fs')
const winners = require('./exports/winners_export_processed.json')

let sortedWinners = winners.sort((a, b) => b.mint - a.mint)

const limit = 0
let counter = 0

const EXPORT_PATH = './exports/'
const IPFS_RENDERS = 'ipfs://bafybeiec32zgd2zn7betyzl4tnbx72gvgxwjlkjy2v5qxhqdqcu7wvhdnq'
const IPFS_STILLS = 'ipfs://bafybeiazpg4ahet2rbarshsehriyxpepaqbpr2zdrmtvy55dzqxsupcs4u'

for (let i in sortedWinners) {
  if (limit && counter >= limit) break

  let vm = sortedWinners[i]

  let vmMeta = {
    description: `Vending Machine that dispenses ${vm.flavor} MoonCatPop`,
    external_url: `https://pop.mooncat.community/vm/${vm.mint}`,
    image: `${IPFS_STILLS}/vm_${vm.mint}.jpg`,
    animation_url: `${IPFS_RENDERS}/vm_${vm.mint}.mp4`,
    name: `${vm.flavor} Vending Machine`,
    attributes: [
      {
        trait_type: 'Machine Id',
        value: String(vm.mint),
      },
      {
        trait_type: 'SpokesCat Rescue Index',
        value: String(vm.rescueOrder),
      },
    ],
  }

  if (typeof vm.name !== 'undefined' && vm.name !== null)
      vmMeta.attributes.push({
        trait_type: 'SpokesCat Name',
        value: `${vm.name}`,
      })

  fs.writeFile(
    EXPORT_PATH + `metadata/${vm.mint}.json`,
    JSON.stringify(vmMeta),
    (err) => {
      if (err) {
        console.error(err)
        return
      }
    }
  )

  for (let c = 0; c < 100; c++) {
    let canId = Number(vm.mint) * 100 + c
    let canMeta = {
      description: `Can of ${vm.flavor} MoonCatPop`,
      external_url: `https://pop.mooncat.community/vm/${vm.mint}`,
      image: `${IPFS_STILLS}/${vm.mint}.jpg`,
      animation_url: `${IPFS_RENDERS}/${vm.mint}.mp4`,
      name: `${vm.flavor}`,
      attributes: [
        {
          trait_type: 'Serial #',
          value: String(canId),
        },
        {
          trait_type: 'Can Id',
          value: String(c),
        },
        {
          trait_type: 'Machine Id',
          value: String(vm.mint),
        },
        {
          trait_type: 'SpokesCat Rescue Index',
          value: String(vm.rescueOrder),
        },
      ],
    }

    if (typeof vm.name !== 'undefined' && vm.name !== null)
      canMeta.attributes.push({
        trait_type: 'SpokesCat Name',
        value: `${vm.name}`,
      })

    if (!fs.existsSync(EXPORT_PATH + `metadata/${vm.mint}`)) {
      fs.mkdirSync(EXPORT_PATH + `metadata/${vm.mint}`)
    }

    fs.writeFile(
      EXPORT_PATH + `metadata/${vm.mint}/${c}.json`,
      JSON.stringify(canMeta),
      (err) => {
        if (err) {
          console.error(err)
          return
        }
      }
    )
  }

  counter += 1
}
