import { ethers } from 'ethers'

const MAX_SUPPLY = 25440

export default class mcbAccessory {
  constructor(id, info, imageData, eligibleList) {
    // info (uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] memory positions, bool availableForPurchase, uint256 price)
    // imageData (bytes2[4] memory positions, bytes8[7] memory palettes, uint8 width, uint8 height, uint8 meta, bytes memory IDAT)
    // eligibleList (bytes32[100] memory)

    this.id = id
    this.availableSupply = info[1] // availableSupply
    this.totalSupply = info[0] // totalSupply
    this.name = this._parseName(info[2])
    this.manager = info[3] // manager
    this.metabyte = info[4] // metabyte
    this.meta = this._parseMetaByte(this.metabyte)
    this.availablePalettes = info[5] // availablePalettes
    this.positions = info[6].map((h) => this._parsePositionHex(h)) // positions
    this.availableForPurchase = info[7] // availableForPurchase
    this.price = ethers.BigNumber.from(info[8]).toString() // price
    this.image = {
      idatHex: imageData[5], // IDAT
      width: imageData[2], // width
      height: imageData[3], // height
      palettes: imageData[1] // palettes
        .map((h) => this._parsePaletteHex(h))
        .filter((p) => p !== false),
    }
    this.eligibleList = eligibleList || []

    this.cache = { info, imageData, eligibleList: this.eligibleList }
    this.fromCache = false
    this.created = null
    this.managerName = null
  }

  isDiscontinued() {
    // @dev - find out what the value of manager would be
    return (
      !this.availableSupply &&
      this.manager === '0x0000000000000000000000000000000000000000'
    )
  }

  isExclusive() {
    return this.isEligibleListActive()
  }

  isLimited() {
    return this.totalSupply < MAX_SUPPLY
  }

  isForSale() {
    return this.availableForPurchase
  }

  isSoldOut() {
    return this.availableSupply === 0
  }

  isVerified() {
    return this.meta.verified
  }

  validPose(index) {
    const poses = { standing: 0, sleeping: 1, pouncing: 2, stalking: 3 }
    return this.positions[poses[index]] !== false
  }

  isEligibleListActive() {
    try {
      return !(parseInt(this.eligibleList[99].slice(-1), 16) % 2 == 0)
    } catch (err) {
      return false
    }
  }

  getAvailablePoses(asName) {
    const poses = { 0: 'standing', 1: 'sleeping', 2: 'pouncing', 3: 'stalking' }
    return this.positions
      .map((position, pose) => {
        return position === false ? position : asName ? poses[pose] : pose
      })
      .filter((pose) => pose !== false)
  }

  hasExclusivePoses() {
    return this.getAvailablePoses().length < 4
  }

  getId() {
    return this.id
  }

  getName() {
    return this.name
  }

  getSlug() {
    return this._formatSlug(this.name)
  }

  getManager(shorten) {
    if (shorten === true) {
      if (this.managerName !== null) return this.managerName
      return this.manager.slice(0, 8)
    }
    return this.manager
  }

  setManagerName(name) {
    if (typeof name !== 'undefined' && name !== null && name.length > 0)
      this.managerName = name
  }

  isManagerVerified() {
    // @dev - decide how to handle verified creators
    return false
  }

  getTotalSupply() {
    return this.totalSupply
  }

  getTotalSold() {
    return this.totalSupply - this.availableSupply
  }

  getRemainingSupply() {
    return this.availableSupply
  }

  displayRemaining() {
    return this.availableSupply + '/' + this.totalSupply
  }

  displayRating() {
    return this.meta.audienceString
  }

  getPrice() {
    return this.price
  }

  getPriceAsEth() {
    if (this.priceIsMax()) return false

    return ethers.utils.formatEther(this.price)
  }

  getPriceAsWei() {
    if (this.priceIsMax()) return false

    return Number(this.price)
    // return ethers.BigNumber.from(this.price).toNumber()
  }

  priceIsMax() {
    return this.price === '4722366482869645213695'
  }

  getAvailablePaletteCount() {
    return this.availablePalettes
  }

  getRandAvailablePose() {
    const availablePoses = this.getAvailablePoses()
    const randomPose =
      availablePoses[Math.floor(Math.random() * availablePoses.length)]
    return ['standing', 'sleeping', 'pouncing', 'stalking'][randomPose]
  }

  getEligibleList() {
    return this.eligibleList
  }

  getBackground() {
    return this.meta.background
  }

  getImageArray(paletteIndex, zIndex, opts) {
    /* 
    glow: (optional) boolean when set, draw glow where applicable (default:true)
    background: (optional) boolean overrides meta attribute
    mirrorPlacement: (optional) boolean overrides meta attribute
    mirrorAccessory: (optional) boolean overrides meta attribute
    */
    opts = opts || {}

    return {
      idat: this.image.idatHex,
      palettes: this.image.palettes,
      zIndex,
      positions: this.positions,
      width: this.image.width,
      height: this.image.height,
      meta: this.metabyte,
      paletteIndex,
      ...opts,
      // background: this.getId() === 2 || this.getId() === 1 ? true : false, // @dev - temp to deal with dummy data
    }
  }

  getCache() {
    return JSON.stringify(this.cache)
  }

  setFromCache(bool) {
    this.fromCache = bool
  }

  getFromCache() {
    return this.fromCache
  }

  setCreated(time) {
    this.created = time
  }

  getCreatedTime() {
    return this.created
  }

  toObject() {
    return {
      id: this.getId(),
      name: this.getName(),
      manager: this.getManager(),
      managerName: this.managerName,
      cache: this.getCache(),
      supply: {
        total: this.totalSupply,
        available: this.availableSupply,
      },
      totalSold: this.totalSupply - this.availableSupply,
      availableForPurchase: this.availableForPurchase,
      soldOut: this.isSoldOut(),
      discontinued: this.isDiscontinued(),
      verified: this.isVerified(),
      price: this.getPriceAsEth(),
      priceInWei: this.getPriceAsWei(),
      pills: {
        exclusive: this.isExclusive(),
        limited: this.isLimited(),
        rating: this.displayRating(),
      },
    }
  }

  _parseName(hex) {
    hex = hex[1] == 'x' ? hex.slice(2) : hex
    let bytes = Uint8Array.from(
      hex
        .replace(/(00)+$/, '')
        .match(/[0-9a-fA-F]{2}/g)
        .map((x) => parseInt(x, 16))
    )
    let decoder = new TextDecoder()
    return decoder.decode(bytes)
  }

  _parseMetaByte(b) {
    const s = ('00000000' + b.toString(2)).slice(-8)
    const reserved = parseInt(s.slice(1, 3), 2)
    const audience = parseInt(s.slice(3, 5), 2)
    return {
      verified: s[0] == '1',
      reserved: reserved,
      audience: audience,
      audienceString: ['Everyone', 'Teen', 'Mature', 'Adult'][audience],
      mirrorPlacement: s[5] == '1',
      mirrorAccessory: s[6] == '1',
      background: s[7] == '1',
    }
  }

  _parsePositionHex(h) {
    if (h === '0xffff') return false
    let position = h.slice(-4).match(/.{2}/g)
    return position.map((b) => parseInt(b, 16))
  }

  _parsePaletteHex(h) {
    if (h === '0x0000000000000000') return false
    let palette = h.slice(-16).match(/.{2}/g)
    return palette.map((b) => parseInt(b, 16))
  }

  _formatSlug(string) {
    string = string.toLowerCase().replace(/\s/g, '-')
    return string.replace(/[^a-z0-9_\-]/g, '')
  }

  static fromCache(id, cache) {
    let data = JSON.parse(cache)
    let accessory = new mcbAccessory(
      id,
      data.info,
      data.imageData,
      data.eligibleList
    )
    accessory.setFromCache(true)
    return accessory
  }
}
