import { ethers } from 'ethers'
import mcbAccessory from '~/lib/accessory'
import { encode, check } from '~/lib/metadata_parser'

import acclimatedABI from './abi/acclimated.json'
import accessoriesABI from './abi/accessories.json'
import raffleABI from './abi/raffle.json'
import factoryABI from './abi/factory.json'
import vmABI from './abi/vending.json'

const ACCLIMATED_ADDR = '0xc3f733ca98e0dad0386979eb96fb1722a1a05e69'
const ACCESSORIES_ADDR = '0x8d33303023723dE93b213da4EB53bE890e747C63'
const FACTORY_ADDR = __NUXT__.config.factoryAddress
const VM_ADDR = __NUXT__.config.vendingAddress
const RAFFLE_ADDR = __NUXT__.config.contractAddress

const INFURA_PROJECTID = __NUXT__.config.infuraId
const INFURA_BASE_URL = 'https://mainnet.infura.io/v3/'

const NON_OWNER_PRICE = 50000000000000000
const MC_OWNER_PRICE = 40000000000000000

export default class mcbDeepSpace {
  constructor() {
    this.provider = window.ethereum
      ? new ethers.providers.Web3Provider(window.ethereum)
      : new ethers.providers.InfuraProvider('mainnet', INFURA_PROJECTID)
  }

  /**
   * Returns the cats owned by a given address
   *
   * @param {address} owner The owner wallet address.
   * @return {Array} Promise of cats by RescueIndex.
   */
  async getAcclimatedCats(owner) {
    const contract = new ethers.Contract(
      ACCLIMATED_ADDR,
      acclimatedABI,
      this.provider
    )
    const tokens = await contract.tokensIdsByOwner(owner)
    return tokens.map((hex) => parseInt(hex))
  }

  /**
   * Returns who is the owner of the given MoonCat
   *
   * @param {number} rescueIndex of the MoonCat.
   * @return {Array} Promise of cats by RescueIndex.
   */
   async ownerOf(rescueIndex) {
    const contract = new ethers.Contract(
      ACCLIMATED_ADDR,
      acclimatedABI,
      this.provider
    )
    const address = await contract.ownerOf(rescueIndex)
    return address
  }

  /**
   * Returns a count of total number of accessories
   *
   * @param {number} id of the accessory.
   * @return {mcbAccessory} Promise of a new mcbAccessory instance.
   * @throws {boolean} false is returned should an error occur.
   */
  async getAccessory(id) {
    try {
      const contract = new ethers.Contract(
        ACCESSORIES_ADDR,
        accessoriesABI,
        this.provider
      )

      const info = await contract.accessoryInfo(id)
      const imageData = await contract.accessoryImageData(id)
      const eligibility = await contract.accessoryEligibleList(id)
      return new mcbAccessory(id, info, imageData, eligibility)
    } catch (err) {
      return false
    }
  }

  /**
   * Returns the number of accessories owned by a given mooncat
   *
   * @return {number} Promise of count of accessories.
   * @throws {number} 0 is returned should an error occur.
   */
  async getCatAccessoryCount(rescueIndex) {
    try {
      const contract = new ethers.Contract(
        ACCESSORIES_ADDR,
        accessoriesABI,
        this.provider
      )
      const count = await contract['balanceOf(uint256)'](rescueIndex)
      return parseInt(count)
    } catch (err) {
      return 0
    }
  }

  /**
   * Returns a cats accessory id by its owned index
   *
   * @return {Object} Promise of the accessory owned.
   * @throws {boolean} false is returned should an error occur.
   */
  async getCatAccessoryByIndex(rescueIndex, accessoryIndex) {
    try {
      const contract = new ethers.Contract(
        ACCESSORIES_ADDR,
        accessoriesABI,
        this.provider
      )
      const ownedAccessory = await contract.ownedAccessoryByIndex(
        rescueIndex,
        accessoryIndex
      )
      return {
        accessoryId: parseInt(ownedAccessory[0]),
        ownedIndex: accessoryIndex,
        paletteIndex: ownedAccessory[1],
        zIndex: ownedAccessory[2],
      }
    } catch (err) {
      return false
    }
  }

  /**
   * Returns whether issuance is Open on the Raffle contract
   *
   * @return {Boolean} Promise of whether issuance is open.
   */
  async issuanceOpen() {
    try {
      const latest = await this.provider.getBlockNumber()
      const contract = new ethers.Contract(
        RAFFLE_ADDR,
        raffleABI,
        this.provider
      )
      const endBlock = await contract.endBlockNumber()

      return endBlock.toNumber() > latest
    } catch (err) {
      return false
    }
    // I believe if ‘startBlock’ is 0 it hasn’t started yet and if ‘increment’ > 0 it’s over
  }

  /**
   * Returns whether the Accessories contract is frozen
   *
   * @return {Boolean} Promise of whether it is frozen.
   */
  async isFrozen() {
    const contract = new ethers.Contract(
      ACCESSORIES_ADDR,
      accessoriesABI,
      this.provider
    )
    const frozen = await contract.frozen()
    return frozen
  }

  /**
   * Issue a ticket based on given valid metadata
   *
   * @param {Object} signer of the wallet.
   * @param {object} metadata unencoded object containing valid metadata.
   * @return {Object} Promise of ticket id.
   * @throws {Object} err is returned should an error occur.
   */
  async issueTicket(signer, metadata) {
    try {
      check(metadata)

      const contract = new ethers.Contract(
        RAFFLE_ADDR,
        raffleABI,
        this.provider
      )
      const asSigner = contract.connect(signer)
      const transaction = await asSigner['issueTicket(bytes)'](
        encode(metadata),
        {
          value: String(900000000000000000),
        }
      )
      return transaction
    } catch (err) {
      console.log(err)
      return err
    }
  }

  /**
   * Get Vending Machine by index
   *
   * @param {Object} index of the vending machine.
   * @return {Object} Promise of vending machine.
   * @throws {Object} err is returned should an error occur.
   */
  async getVM(index) {
    try {
      const contract = new ethers.Contract(
        FACTORY_ADDR,
        factoryABI,
        this.provider
      )

      const vm = await contract.moonCatVendingMachines(index)
      
      return vm
    } catch (err) {
      console.log(err)
      return err
    }
  }

  /**
   * Get remaining supply of a vending machine
   *
   * @param {Object} index of the vending machine.
   * @return {Number} Number of cans available to mint.
   * @throws {Object} err is returned should an error occur.
   */
  async getRemainingSupply(index) {
    try {
      const contract = new ethers.Contract(VM_ADDR, vmABI, this.provider)
      
      const supply = await contract.totalSupplyPerMachine(index)

      return 100 - supply.toNumber()
    } catch (err) {
      console.log(err)
      return false
    }
  }

  /**
   * Get Metadata of a vending machine
   *
   * @param {Object} index of the vending machine.
   * @return {Number} Number of cans available to mint.
   * @throws {Object} err is returned should an error occur.
   */
  async getMetadata(index) {
    try {
      const contract = new ethers.Contract(
        FACTORY_ADDR,
        factoryABI,
        this.provider
      )

      const metadata = await contract.getVendingMachineMetadata(index)

      return metadata
    } catch (err) {
      console.log(err)
      return false
    }
  }

  /**
   * Get Owner of a vending machine
   *
   * @param {Object} index of the vending machine.
   * @return {address} Address of owner of the vending machine.
   * @throws {Object} err is returned should an error occur.
   */
   async getVMOwner(index) {
    try {
      const contract = new ethers.Contract(
        FACTORY_ADDR,
        factoryABI,
        this.provider
      )

      const address = await contract.ownerOf(index)

      return address
    } catch (err) {
      console.log(err)
      return false
    }
  }

  /**
   * Get Block when a vending machine can start minting
   *
   * @param {Object} index of the vending machine.
   * @return {Number} Block number when cans are available to mint.
   * @throws {Object} err is returned should an error occur.
   */
  async getMintStart(index) {
    try {
      const contract = new ethers.Contract(
        FACTORY_ADDR,
        factoryABI,
        this.provider
      )
      
      const startBlock = await contract.vendingMachineCanMintStart(index)

      return startBlock.toNumber()
    } catch (err) {
      console.log(err)
      return 0
    }
  }

  /**
   * Get whether minting is open
   *
   * @param {Object} index of the vending machine.
   * @return {Boolean} Boolean of whether cans are available to mint.
   * @throws {Object} err is returned should an error occur.
   */
  async isMintingOpen(index) {
    try {
      const latest = await this.provider.getBlockNumber()
      const contract = new ethers.Contract(
        FACTORY_ADDR,
        factoryABI,
        this.provider
      )

      const startBlock = await contract.vendingMachineCanMintStart(index)
      
      if(startBlock.toNumber() === 0) return false

      return startBlock <= latest
    } catch (err) {
      console.log(err)
      return false
    }
  }

  /**
   * Mint a single can by vending machine id
   *
   * @param {Object} signer of the wallet.
   * @param {Number} vmId of the vending machine.
   * @param {Boolean} mcOwner indicator wallet owns MoonCats.
   * @return {Object} Promise of vending machine.
   * @throws {Object} err is returned should an error occur.
   */
  async mintCan(signer, vmId, mcOwner) {
    try {
      const contract = new ethers.Contract(VM_ADDR, vmABI, this.provider)
      const asSigner = contract.connect(signer)
      const transaction = await asSigner['mint(uint256)'](vmId, {
        value: String(mcOwner ? MC_OWNER_PRICE : NON_OWNER_PRICE),
      })
      return transaction
    } catch (err) {
      console.log(err)
      return err
    }
  }

  /**
   * Mint a batch of cans by array of vending machine ids
   *
   * @param {Object} signer of the wallet.
   * @param {Array[Number]} [vmIdArray] of the vending machines.
   * @param {Boolean} mcOwner indicator wallet owns MoonCats.
   * @return {Object} Promise of vending machine.
   * @throws {Object} err is returned should an error occur.
   */
  async batchMintCans(signer, vmIdArray, mcOwner) {
    try {
      const contract = new ethers.Contract(VM_ADDR, vmABI, this.provider)
      const asSigner = contract.connect(signer)
      const transaction = await asSigner['batchMint(uint256[])'](vmIdArray, {
        value: String(
          (mcOwner ? MC_OWNER_PRICE : NON_OWNER_PRICE) * vmIdArray.length
        ),
      })
      return transaction
    } catch (err) {
      console.log(err)
      return err
    }
  }
}

// issueTicket(bytes calldata metadata)
// isWinner (uint256 ticketId)
