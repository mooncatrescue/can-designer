let PopNameGenerator = (function () {
  let brand = [
    "Moo's",
    "Paws'",
    "Tusk's",
    "Mark's",
    "Whiskers'",
    "Midnight's",
    "Wander's",
    "0's",
  ]

  let honorific = [
    'Best',
    'Choice',
    'Famous',
    'Paw-Brewed',
    'Old Fashioned',
    'Blue Ribbon',
    'Organic',
  ]

  let type = [
    'Diet',
    'Extreme',
    'Ice Cold',
    'Double-Nipped',
    'Triple-Nipped',
    'Pawsome',
    'Sedating',
    'Fair Trade',
  ]

  let prefix = [
    'Genesis',
    'Ponder',
    'Fruity',
    'Fizzy',
    'Frosty',
    'Spotted',
    'Sparkly',
    'Panther',
    'Pixelator',
    'TomCat',
    'Cougar',
    'Pretty Kitty',
    'Lion',
    'Fat Rat',
    'Dr.',
    'Scratching Post',
    'Puma',
    'Asteroid',
    'Milky Way',
    'Cosmos',
    'Celestial',
    'Deep Space',
    'Eclipse',
    'Interstellar',
    'Star',
    'Sour',
    'Catty',
    'Mostly',
    'Groomer',
    'Quest',
    'Black Hole',
    'Parallax',
  ]

  let beverage = [
    'Furball',
    'Nip',
    'Bubbles',
    'Stardust',
    'Soda',
    'Cola',
    'Pop',
    'Ale',
    'Explosion',
    'Madness',
    'Punch',
    'Tonic',
    'Sparkle',
    'Mane',
    'Nectar',
    'Juice',
    'Refresher',
    'Quench',
    'Blast',
    'Pucker',
    'Pixel',
    'Malt',
    'Joose',
    'Fever',
    'Milk',
    'Cream',
    'Comet',
    'Elixer',
  ]

  function selectRandom(arr) {
    return arr[Math.floor(Math.random() * arr.length)]
  }

  function segment(arr, probability) {
    if (Math.random() <= probability) {
      return ' ' + selectRandom(arr)
    } else {
      return ''
    }
  }

  function generate() {
    var name =
      segment(brand, 0.3) + segment(honorific, 0.3) + segment(type, 0.3)
    if (name.length == 0) {
      name += segment(prefix, 1)
    } else {
      name += segment(prefix, 0.8)
    }
    return (name + segment(beverage, 1)).trim()
  }

  return generate
})()

/*for(var i = 0; i < 100; i++) {
  console.log(PopNameGenerator());
}
*/

if (typeof module === 'object') {
  module.exports = PopNameGenerator
}
