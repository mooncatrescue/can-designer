import * as THREE from 'three'
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

const API_URL = __NUXT__.config.apiURL

export default class mcpPreviewer {
  constructor() {
    this.scene = null
    this.directionalLight = null
    this.renderer = null
    this.camera = null
    this.controls = null
    this.aspect = 0
    this.el = null
    this.metadata = null
  }

  setElement(el) {
    this.el = el
  }

  init(el) {
    if (typeof el !== 'undefined') this.el = el

    if (this.el === null) return

    this.scene = new THREE.Scene()

    // Add a cube to the scene
    const geometry = new THREE.BoxGeometry(0.1, 0.1, 0.1) // width, height, depth
    const material = new THREE.MeshLambertMaterial({ color: 0xfb8e00 })
    const mesh = new THREE.Mesh(geometry, material)
    mesh.position.set(0, 0, 0)
    // this.scene.add(mesh)

    // Set up lights
    const ambientLight = new THREE.AmbientLight(0xffffff, 0.6)
    this.scene.add(ambientLight)

    this.directionalLight = new THREE.DirectionalLight(0xffffff, 0.5)
    this.directionalLight.position.set(1, 0.5, 0) // x, y, z
    this.scene.add(this.directionalLight)

    // Camera
    this.aspect = this.el.offsetWidth / this.el.offsetHeight
    this.camera = new THREE.PerspectiveCamera(
      45, // field of view in degrees
      this.aspect, // aspect ratio
      0.1, // near plane
      100 // far plane
    )

    this.camera.position.set(0.2, 0.15, 0.2)
    // this.camera.lookAt(0, 0, 0);
    // this.camera.lookAt(1, 1, 1);

    // Renderer
    this.renderer = new THREE.WebGLRenderer({ antialias: true })
    this.renderer.setSize(this.el.offsetWidth - 16, this.el.offsetHeight - 16)

    // Controls
    this.controls = new OrbitControls(this.camera, this.renderer.domElement)
    this.controls.target.set(0, 0, 0)

    this.controls.minDistance = 0.2
    this.controls.maxDistance = 0.4

    window.addEventListener('resize', this.resize.bind(this))

    // Append to Element (el)
    this.el.innerHTML = ''
    this.el.appendChild(this.renderer.domElement)
  }

  loadTexture(image, tabColor) {
    if (this.el === null) return

    // // Create an image
    // const image = new Image() // or document.createElement('img' );
    // // Create texture
    // var texture = new THREE.Texture(image)
    // // On image load, update texture
    // image.onload = () => {
    //   texture.needsUpdate = true
    // }
    // // Set image source
    // image.src = 'data:image/png;base64,' + data // 'data:image/png;base64,XXXXX'
    const textureLoader = new THREE.TextureLoader()
    var texture = textureLoader.load(image)
    // var texture = THREE.ImageUtils.loadTexture('data:image/png;base64,' + image)

    const can_wrap = new THREE.MeshPhongMaterial({
      map: texture,
      specular: new THREE.Color(0x555555),
    })
    const can_body = new THREE.MeshPhongMaterial({
      map: texture,
      specular: new THREE.Color(0xffffff),
    })
    const can_tab = new THREE.MeshPhongMaterial({
      color: Number(tabColor), // Set to Coat color
      specular: new THREE.Color(0x555555),
    })

    const OBJloader = new OBJLoader()

    OBJloader.load(
      '/can.obj',
      function (object) {
        // Called once the object is loaded
        object.children[0].material = can_wrap
        object.children[1].material = can_tab
        object.children[2].material = can_body

        object.position.set(0, -0.05, -0.129)

        this.scene.add(object)
        this.renderer.render(this.scene, this.camera)
      }.bind(this),
      // called when loading is in progresses
      function (xhr) {
        // console.log((xhr.loaded / xhr.total) * 100 + '% loaded')
      },
      // called when loading has errors
      function (error) {
        console.log('An error happened')
      }
    )
  }

  animate() {
    if (this.el === null) return

    requestAnimationFrame(this.animate.bind(this))
    this.controls.update()
    this.directionalLight.position.set(
      this.camera.position.x,
      this.camera.position.y,
      this.camera.position.z
    )
    this.renderer.render(this.scene, this.camera)
  }

  resize() {
    this.renderer.setSize(
      this.el.offsetWidth - 16,
      (this.el.offsetWidth - 16) / this.aspect
    )
  }

  destroy() {
    try {
      this.el.innerHTML = '<div class="Builder-previewLoader"></div>'
    } catch (err) {}
    window.removeEventListener('resize', this.resize.bind(this))
  }

  fetch(data) {
    if (JSON.stringify(data) === JSON.stringify(this.metadata)) return

    let accessories = [...data.accessories].flat().join(',')

    if (accessories.length === 0) accessories = 0

    const url = `${API_URL}/pop-texture?id=${
      data.id
    }&accessories=${accessories}&color=${data.color}&textcolor=${
      data.textcolor
    }&textshadow=${data.textshadow}&scale=${
      data.scale
    }&flavor=${encodeURIComponent(data.flavor)}&gradient=${
      data.gradient
    }&pattern=${data.pattern}`

    this.loadTexture(url, this._rgbToHex(data.tab))

    this.metadata = data
  }

  _rgbToHex(rgbArr) {
    return (
      '0x' +
      ((1 << 24) + (rgbArr[0] << 16) + (rgbArr[1] << 8) + rgbArr[2])
        .toString(16)
        .slice(1)
    )
  }
}
