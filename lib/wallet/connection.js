import { ethers } from 'ethers'
import WalletConnectProvider from '@walletconnect/web3-provider'
import WalletLink from 'walletlink'
import Web3Modal from 'web3modal'

const INFURA_PROJECTID = __NUXT__.config.infuraId

export default class Connection {
  constructor(network, cacheProvider) {
    const providerOptions = {
      walletconnect: {
        package: WalletConnectProvider,
        options: {
          infuraId: INFURA_PROJECTID,
        },
      },
      'custom-coinbase': {
        display: {
          logo: '/coinbase.svg',
          name: 'Coinbase',
          description: 'Scan with WalletLink to connect',
        },
        options: {
          appName: 'mooncatpop', // Your app name
          networkUrl: `https://mainnet.infura.io/v3/${INFURA_PROJECTID}`,
          chainId: 'mainnet',
        },
        package: WalletLink,
        connector: async (_, options) => {
          const { appName, networkUrl, chainId } = options
          const walletLink = new WalletLink({
            appName,
          })
          const provider = walletLink.makeWeb3Provider(networkUrl, chainId)
          await provider.enable()
          return provider
        },
      },
    }

    this.connected = false
    this.provider = null
    this.address = null
    this.onChange = () => {}

    this.web3Modal = new Web3Modal({
      network: network || 'mainnet', // optional
      cacheProvider: cacheProvider || true, // optional
      providerOptions, // required
    })
  }

  async init() {
    if (this.web3Modal.cachedProvider) await this._onConnect()
  }

  isConnected() {
    return this.connected
  }

  getAddress() {
    return this.address
  }

  getSigner() {
    const provider = new ethers.providers.Web3Provider(this.provider)
    return provider.getSigner()
  }

  connect() {
    return new Promise(async (resolve, reject) => {
      if (!this.connected) await this._onConnect()
      resolve(this.connected)
    })
  }

  disconnect() {
    return new Promise(async (resolve, reject) => {
      if (this.connected) await this._onDisconnect()
      resolve(this.connected)
    })
  }

  _setConnected(bool) {
    this.connected = bool
    this.onChange(this.connected)
  }

  async _onConnect() {
    try {
      this.provider = await this.web3Modal.connect()
      await this.fetchAccount()
    } catch (err) {
      console.log('Could not get a wallet connection', err)
      return
    }

    this.provider.on('accountsChanged', (accounts) => this.fetchAccount())
    this.provider.on('chainChanged', (chainId) => this.fetchAccount())
  }

  async _onDisconnect() {
    // @dm Which providers have close method?
    if (this.provider.close) {
      await this.provider.close()

      // If the cached provider is not cleared,
      // WalletConnect will default to the existing session
      // and does not allow to re-scan the QR code with a new wallet.
      // Depending on your use case you may want or want not his behavir.
    }

    await this.web3Modal.clearCachedProvider()
    this.provider = null

    this.address = null
    this._setConnected(false)
  }

  fetchAccount() {
    try {
      const provider = new ethers.providers.Web3Provider(this.provider)
      const signer = provider.getSigner()

      return signer
        .getAddress()
        .then((addr) => {
          this.address = addr
          this._setConnected(true)
        })
        .catch((err) => this._onDisconnect())
    } catch (err) {
      this._onDisconnect()
    }
  }
}
