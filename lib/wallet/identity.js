import mcbConnection from '~/lib/wallet/connection'
import mcbDeepSpace from '~/lib/deepspace'

export default class mcbIdentity {
  constructor(store) {
    this.store = store
    this.connection = new mcbConnection()
    this.dsn = new mcbDeepSpace()

    this.loadingAcclimated = false
    this.dsn.issuanceOpen().then((isOpen) => {
      this.store.dispatch('identity/setOpen', { open: isOpen })
    })

    this.connection.onChange = (connected) => {
      this.store.dispatch('identity/connectionChange', {
        connected,
        address: this.connection.getAddress(),
      })
      if (connected) this.getAcclimatedCats()
    }
  }

  init() {
    return this.connection.init()
  }

  getAcclimatedCats() {
    if (!this.connection.isConnected() || this.loadingAcclimated) return

    this.loadingAcclimated = true

    this.dsn
      .getAcclimatedCats(this.connection.getAddress())
      .then((cats) => {
        let catAccessoryPromises = []

        for (let i = 0; i < cats.length; i++) {
          catAccessoryPromises[i] = new Promise((resolve, reject) => {
            this.dsn
              .getCatAccessoryCount(cats[i])
              .then((count) => {
                let accessoryPromises = []

                if (count > 0) {
                  for (let a = 0; a < count; a++) {
                    accessoryPromises[a] = new Promise((resolve, reject) => {
                      this.dsn
                        .getCatAccessoryByIndex(cats[i], a)
                        .then((accessory) => {
                          resolve(accessory)
                        })
                        .catch((err) => {})
                    })
                  }
                }

                Promise.all(accessoryPromises)
                  .then((accessories) => {
                    resolve({
                      rescueIndex: cats[i],
                      count,
                      accessories: accessories
                        .filter((accessory) => accessory !== false)
                        .reduce((map, obj) => {
                          map[obj.accessoryId] = obj
                          return map
                        }, {}),
                    })
                  })
                  .catch((err) => {})
              })
              .catch((err) => {})
          })
        }

        Promise.all(catAccessoryPromises)
          .then((ownership) => {
            let accessories = new Set()
            for (let i in ownership) {
              for (let a in ownership[i].accessories) {
                if (ownership[i].accessories[a] !== false)
                  accessories.add(ownership[i].accessories[a].accessoryId)
              }
            }

            let accessoryArray = Array.from(accessories)
            let loadAccessories = []
            for (let i in accessoryArray) {
              loadAccessories[i] = new Promise((resolve, reject) => {
                this.dsn
                  .getAccessory(accessoryArray[i])
                  .then((result) => {
                    resolve(result)
                  })
                  .catch((err) => {})
              })
            }

            Promise.all(loadAccessories)
              .then((accessories) => {
                this.store
                  .dispatch('accessories/addAccessories', accessories)
                  .then(() => {
                    this.store.dispatch('identity/setFullOwnership', ownership)
                  })
                  .catch((err) => {})
              })
              .catch((err) => {
                this.store.dispatch('identity/setFullOwnership', ownership)
              })
          })
          .catch((err) => {})

        this.store.dispatch('identity/setCats', { cats })
        this.loadingAcclimated = false
      })
      .catch((err) => {})
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.connection.connect().then((connected) => {
        resolve(connected)
      })
    })
  }

  disconnect() {
    return new Promise((resolve, reject) => {
      this.connection.disconnect().then((connected) => {
        resolve(connected)
      })
    })
  }
}
