import { mapState, mapGetters } from 'vuex'

export default {
  data() {
    return {}
  },

  computed: {
    ...mapState({}),
    ...mapGetters({
      isLibLoaded: 'mooncat/isLibLoaded',
      catContext: 'identity/catContext',
      getCatImageData: 'mooncat/getCatImageData',
      getCatId: 'mooncat/getCatId',
      randomRescueId: 'mooncat/randomRescueId',
      randomCatImageDataByPose: 'mooncat/randomCatImageDataByPose',
      randomCatIdByPose: 'mooncat/randomCatIdByPose',
      isEligible: 'mooncat/isEligible',
      hasValidPose: 'mooncat/hasValidPose',
      getMoonCatPalette: 'mooncat/getMoonCatPalette',
      getCatTraits: 'mooncat/getCatTraits',
    }),
    hasCatContext() {
      return this.catContext !== null
    },
  },

  watch: {
    isLibLoaded() {
      this.$forceUpdate()
    },
  },

  methods: {},
}
