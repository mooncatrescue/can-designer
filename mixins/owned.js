import { mapState, mapGetters } from 'vuex'

export default {
  data() {
    return { ownedAccessories: {} }
  },

  computed: {
    ...mapGetters({
      cats: 'identity/cats',
      ownership: 'identity/ownership',
    }),
  },

  watch: {
    cats() {
      // When a new cat is create accessory list array
      this.createAccessoryList()
    },
    ownership() {
      this.checkOwnership()
    },
  },

  beforeCreate() {
    // Get the identities Acclimated Cats (refresh)
    this.$nuxt.context.$identity.getAcclimatedCats()
  },

  methods: {
    createAccessoryList() {
      // For each cat create an empty array for to hold it's accessory ownership objects
      try {
        for (let i in this.cats) {
          let rescueIndex = this.cats[i]
          this.ownedAccessories = {
            ...this.ownedAccessories,
            [rescueIndex]: [],
          }
        }
      } catch (err) {}
    },
    checkOwnership() {
      let ownershipArray = Object.values(this.ownership)
      // Add accessory ownership objects to array(s)
      for (let i in ownershipArray) {
        let ownership = ownershipArray[i]
        this.ownedAccessories = {
          ...this.ownedAccessories,
          [ownership.rescueIndex]: Object.values(ownership.accessories),
        }
      }
    },
  },
}
