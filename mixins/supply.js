import { mapState, mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      cats: 'identity/cats',
    }),
  },

  methods: {
    supplyLabel(supply) {
      switch (this.supplyLevel(supply)) {
        case 3:
          return 'High'
        case 2:
          return 'Medium'
        case 1:
          return 'Low'
        default:
          return 'Sold Out'
      }
    },
    supplyLevel(supply) {
      switch (true) {
        case supply === 0:
          return 0
        case supply > 60:
          return 3
        case supply > 25 && supply < 60:
          return 2
        case supply <= 25:
          return 1
      }
    },
  },
}
