import mcbIdentity from '~/lib/wallet/identity'

export default (context) => {
  const { store } = context
  context.$identity = new mcbIdentity(store)
  if (context.$config.stage === 'dev') window.mcbIdentity = context.$identity

  return new Promise(async (resolve, reject) => {
    context.$identity.init().then(() => {
      resolve()
    })
  })
}
