const STORAGE_KEY = 'MCPBASKET'

export default (context) => {
  const { store } = context

  store.subscribe((mutation, state) => {
    if (
      [
        'basket/addItem',
        'basket/removeItem',
        'basket/oneMoreItem',
        'basket/oneLessItem',
      ].includes(mutation.type)
    ) {
      localStorage.setItem(STORAGE_KEY, JSON.stringify(state.basket.items))
    }
  })
}
