import { initializeApp, getApp } from 'firebase/app'
import { getFirestore, collection } from 'firebase/firestore'
import {
  getFunctions,
  connectFunctionsEmulator,
  httpsCallable,
} from 'firebase/functions'

const isDev = __NUXT__.config.stage === 'dev'

// if (!firebase.apps.length) firebase.initializeApp(config)

const firebaseApp = initializeApp({
  apiKey: 'AIzaSyDTH9syVatwv3sMVg6_bgjfz9pODSfffso',
  authDomain: 'mooncat-can-designer.firebaseapp.com',
  projectId: 'mooncat-can-designer',
  storageBucket: 'mooncat-can-designer.appspot.com',
  messagingSenderId: '366110626176',
  appId: '1:366110626176:web:f59c7dc3ed2306fbeff165',
  measurementId: 'G-3LSDZ5B2Z5',
})

export const db = getFirestore()
export const functions = getFunctions(firebaseApp)
export const callable = httpsCallable

if (isDev) {
  connectFunctionsEmulator(functions, 'localhost', 5001)
}

// firebase collections
export const ticketsCollection = collection(
  db,
  isDev ? 'dev-tickets' : 'tickets'
)
export const machinesCollection = collection(
  db,
  isDev ? 'dev-machines' : 'machines'
)
