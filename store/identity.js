import mcbLog from '~/lib/logger'

mcbLog.setContext('store/identity')

export const state = () => ({
  open: true,
  address: null,
  connected: false,
  cats: [],
  accessories: [],
  offBy: '',
  ownership: {},
  catContext: null,
  txOutcome: false,
  txRecord: '',
})

export const getters = {
  isOpen(state) {
    return state.open
  },
  isConnected(state) {
    return state.connected
  },
  shortAddress(state) {
    return state.address
      ? state.address.substring(0, 6) +
          '...' +
          state.address.substring(state.address.length - 4)
      : ''
  },
  fullAddress(state) {
    return state.address
  },
  cats(state) {
    return Array.from(state.cats).sort(function (a, b) {
      return a - b
    })
  },
  catContext(state) {
    return state.catContext
  },
  offBy(state) {
    return state.offBy
  },
  ownership(state) {
    return state.ownership
  },
  txOutcome(state) {
    return state.txOutcome
  },
  txRecord(state) {
    return state.txRecord
  },
}

export const mutations = {
  open(state, bool) {
    state.open = bool
  },
  connect(state, bool) {
    state.connected = bool
    if (state.connected) console.log('Welcome to MoonCatPop.')
  },
  offBy(state, payload) {
    state.offBy = payload
  },
  setAddress(state, address) {
    state.address = address
    mcbLog.info(`Set wallet address to ${address}`)
  },
  setCats(state, cats) {
    state.cats = cats
    mcbLog.info(`Set cats`, cats)
  },
  setCatContext(state, id) {
    state.catContext = id
    mcbLog.info(`Set cat context to #${id}`)
  },
  setAccessoryOwnedCount(state, payload) {
    let ownership =
      payload.rescueIndex in state.ownership
        ? state.ownership[payload.rescueIndex]
        : {
            rescueIndex: payload.rescueIndex,
            count: 0,
            accessories: {},
          }

    ownership.count = payload.count

    state.ownership = { ...state.ownership, [payload.rescueIndex]: ownership }

    mcbLog.info(
      `Set accessory owned count to ${payload.count} for MoonCat #${payload.rescueIndex}`,
      payload
    )
  },
  addAccessoryOwnership(state, payload) {
    let id = payload.accessory.accessoryId
    if (!state.accessories.includes(id)) state.accessories.push(id)

    let ownership =
      payload.rescueIndex in state.ownership
        ? state.ownership[payload.rescueIndex]
        : {
            rescueIndex: payload.rescueIndex,
            count: 0,
            accessories: {},
          }

    ownership.accessories[id] = { ...payload.accessory, owned: true }
    ownership.count = 0
    if (ownership.count === 0)
      ownership.count = Object.keys(ownership.accessories).length

    state.ownership = { ...state.ownership, [payload.rescueIndex]: ownership }

    mcbLog.info(
      `Add accessory ownership of #${id} for MoonCat #${payload.rescueIndex}`,
      payload
    )
  },
  setFullOwnership(state, payload) {
    let ownership = payload.reduce((map, obj) => {
      map[obj.rescueIndex] = obj
      return map
    }, {})
    state.ownership = { ...ownership }
    mcbLog.info(`Set full accessory ownership`, payload)
  },
  txOutcome(state, payload) {
    state.txOutcome = payload
  },
  txRecord(state, payload) {
    state.txRecord = payload
  },
  updateAccessory(state, payload) {
    // Check whether the cat is in ownership object
    // and the accessory is in the ownership accessories object
    if (
      typeof state.ownership[payload.rescueIndex] !== 'undefined' &&
      typeof state.ownership[payload.rescueIndex].accessories[
        payload.accessoryId
      ] !== 'undefined'
    ) {
      let rescueIndex = payload.rescueIndex
      let ownership = state.ownership[payload.rescueIndex]
      let accessories = { ...ownership.accessories }
      delete payload.rescueIndex

      ownership.accessories[payload.accessoryId] = {
        ...accessories[payload.accessoryId],
        ...payload,
      }
      state.ownership = { ...state.ownership, [rescueIndex]: ownership }

      mcbLog.info(
        `Update accessory ownership of #${payload.accessoryId} for MoonCat #${rescueIndex}`,
        payload
      )
    } else {
      mcbLog.warning(
        `Tried to update accessory ownership of #${payload.accessoryId} for MoonCat #${rescueIndex}, but ownership not present`,
        payload
      )
    }
  },
  updateIndexes(state, payload) {
    let rescueIndex = payload.rescueIndex
    let ownership = state.ownership[rescueIndex]
    let accessories = { ...ownership.accessories }

    for (let idx in payload.indexes) {
      let accessoryId = payload.indexes[idx].accessoryId
      if (typeof accessories[accessoryId] !== 'undefined') {
        // If the accessory was found, and the zIndex has changed, update the accessory object
        if (accessories[accessoryId].zIndex !== payload.indexes[idx].zIndex) {
          ownership.accessories[accessoryId] = {
            ...ownership.accessories[accessoryId],
            zIndex: payload.indexes[idx].zIndex,
          }
        }
      } else {
        mcbLog.warning(
          `Tried to update indexes of MoonCat #${rescueIndex} for accessory #${accessoryId}, but accessory ownership not found`,
          payload
        )
      }
    }

    state.ownership = { ...state.ownership, [rescueIndex]: ownership }

    mcbLog.info(
      `Update accessory indexes of for MoonCat #${rescueIndex}`,
      payload
    )
  },
}

export const actions = {
  setOpen({ commit }, payload) {
    commit('open', payload.open)
  },
  connectionChange({ dispatch, commit, state }, payload) {
    return new Promise((resolve, reject) => {
      if (
        typeof payload.connected !== 'undefined' &&
        payload.connected !== state.connected
      )
        commit('connect', payload.connected)
      if (
        typeof payload.address !== 'undefined' &&
        payload.address !== state.address
      )
        commit('setAddress', payload.address)

      if (payload.connected === false) {
        commit('setCats', [])
        commit('setCatContext', null)
      }
      resolve()
    })
  },
  offBy({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('offBy', payload)
      resolve()
    })
  },
  setCats({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('setCats', payload.cats)
      resolve()
    })
  },
  setCatContext({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('setCatContext', payload.id)
      resolve()
    })
  },
  setAccessoryCount({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('setAccessoryOwnedCount', payload)
      resolve()
    })
  },
  setAccessoryOwned({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('addAccessoryOwnership', payload)
      resolve()
    })
  },
  setFullOwnership({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('setFullOwnership', payload)
      resolve()
    })
  },
  txOutcome({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('txOutcome', payload)
      resolve()
    })
  },
  txRecord({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('txRecord', payload)
      resolve()
    })
  },
  updateAccessory({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('updateAccessory', payload)
      resolve()
    })
  },
  updateIndexes({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('updateIndexes', payload)
      resolve()
    })
  },
}
