import mcbLog from '~/lib/logger'

mcbLog.setContext('store/mooncat')

const TOTAL_CATS = 25440
const DEFAULT_CAT_IMAGEDATA =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAArIAAAKyCAYAAAApeT2AAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAotSURBVHgB7doxalxJFEbhW+NexDCJjWDWIPD+MWgNBiEn3oUpO1QkOakW5/F9SefFDU7/vBkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKBtDcA79t6Pc8Ba62nIcx/AR/lnAAAgSMgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAk3Qa4jL3348BFnLrntdbTAJcgZIG/8WMOECr3dfCPzpH7+OPzALzBpwUAACQJWQAAkoQsAABJQhYAgCQhCwBAkpAFACBJyAIAkCRkAQBIErIAACQJWQAAkoQsAABJQhYAgCQhCwBAkpAFACBJyAIAkCRkAQBIErIAACQJWQAAkoQsAABJQhYAgCQhCwBA0m2AK/k0LT/mgL33v39+HobXnufQex9Uu2fgziyyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQNIa4O723o9zxqc543ng/h7mjF9zwFrraYC7ssgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAICk2wBX8jxwHafu+fMAl2CRBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASbcB+Fj/zxnfh9e8M3A5FlkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAg6TYAH+v7cA/eGbgciywAAElCFgCAJCELAECSkAUAIEnIAgCQJGQBAEgSsgAAJAlZAACShCwAAElCFgCAJCELAECSkAUAIEnIAgCQJGQBAEgSsgAAJAlZAACShCwAAElCFgCAJCELAECSkAUAIEnIAgCQJGQBAEi6DcD7vkzPy5zz35zxc875Mj0vA/AGiywAAElCFgCAJCELAECSkAUAIEnIAgCQJGQBAEgSsgAAJAlZAACShCwAAElCFgCAJCELAECSkAUAIEnIAgCQJGQBAEgSsgAAJAlZAACShCwAAElCFgCAJCELAECSkAUAIEnIAgCQdBuA970Mr/2cnpcBuBiLLAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASFoDXMbe++uc8Txwfw9zwFrr2wCXYJEFACBJyAIAkCRkAQBIErIAACQJWQAAkoQsAABJQhYAgCQhCwBAkpAFACBJyAIAkCRkAQBIErIAACQJWQAAkoQsAABJQhYAgCQhCwBAkpAFACBJyAIAkCRkAQBIErIAACQJWQAAktYAvGPv/ThwZ2utpwF4g0UWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEASBKyAAAkCVkAAJKELAAASUIWAIAkIQsAQJKQBQAgScgCAJAkZAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA+AC/AaZ8OlapdWCtAAAAAElFTkSuQmCC'

export const state = () => ({
  libLoaded: false,
})

export const getters = {
  isLibLoaded(state) {
    return state.libLoaded
  },
  getCatId: (state) => (id) => {
    if (!state.libLoaded || typeof id === 'undefined') return

    return $nuxt.context.$mooncat.lib.getCatId(id)
  },
  getCatTraits: (state) => (id, type) => {
    if (!state.libLoaded || typeof id === 'undefined') return
    type = type || 'basic'

    return $nuxt.context.$mooncat.lib.getTraits(type, id)
  },
  getCatImageData: (state, getters) => (id, accessories, opts) => {
    if (!state.libLoaded) return DEFAULT_CAT_IMAGEDATA

    if (id === null || typeof id === 'undefined') id = getters.randomRescueId

    accessories = accessories || []
    opts = opts || {
      fullSize: true,
      scale: 4,
      padding: 10,
    }

    const catId =
      typeof id !== 'undefined' &&
      id !== null &&
      typeof id !== 'number' &&
      id.substr(0, 2) === '0x'
        ? id
        : $nuxt.context.$mooncat.lib.getCatId(id)
    const catTraits = $nuxt.context.$mooncat.lib.getTraits('basic', catId)
    const poses = { standing: 0, sleeping: 1, pouncing: 2, stalking: 3 }

    for (let i = 0; i < accessories.length; i++) {
      if (accessories[i].positions[poses[catTraits.pose]] === false)
        return DEFAULT_CAT_IMAGEDATA
    }

    return $nuxt.context.$mooncat.lib.generateImage(catId, accessories, opts)
  },
  randomRescueId(state) {
    return Math.floor(Math.random() * TOTAL_CATS)
  },
  randomCatImageDataByPose: (state) => (pose, accessories, opts) => {
    if (!state.libLoaded) return DEFAULT_CAT_IMAGEDATA

    const def = {
      hue: Math.floor(Math.random() * (0 - 360) + 360),
      expression: ['smiling', 'grumpy', 'pouting', 'shy'][
        Math.floor(Math.random() * 4)
      ],
      pattern: ['pure', 'tabby', 'spotted', 'tortie'][
        Math.floor(Math.random() * 4)
      ],
      pose,
      facing: ['left', 'right'][Math.floor(Math.random() * 2)],
      pale: Math.random() < 0.5,
    }

    accessories = accessories || []
    opts = opts || {
      fullSize: true,
      scale: 4,
      padding: 10,
    }

    const catId = $nuxt.context.$mooncat.lib.generateMoonCatId(def)
    return $nuxt.context.$mooncat.lib.generateImage(catId, accessories, opts)
  },
  randomCatIdByPose: (state) => (pose) => {
    const def = {
      hue: Math.floor(Math.random() * (0 - 360) + 360),
      expression: ['smiling', 'grumpy', 'pouting', 'shy'][
        Math.floor(Math.random() * 4)
      ],
      pattern: ['pure', 'tabby', 'spotted', 'tortie'][
        Math.floor(Math.random() * 4)
      ],
      pose,
      facing: ['left', 'right'][Math.floor(Math.random() * 2)],
      pale: Math.random() < 0.5,
    }

    const catId = $nuxt.context.$mooncat.lib.generateMoonCatId(def)
    return catId
  },
  isEligible: (state) => (id, eligibilityList) => {
    if (!state.libLoaded) return false

    return $nuxt.context.$mooncat.lib.isEligible(eligibilityList, id)
  },
  hasValidPose: (state) => (id, availablePoses) => {
    if (!state.libLoaded) return false

    const traits = $nuxt.context.$mooncat.lib.getTraits('basic', id)
    return availablePoses.includes(traits.pose)
  },
  getMoonCatPalette: (state) => (id) => {
    if (!state.libLoaded) return false

    const catId =
      typeof id !== 'undefined' &&
      id !== null &&
      typeof id !== 'number' &&
      id.substr(0, 2) === '0x'
        ? id
        : $nuxt.context.$mooncat.lib.getCatId(id)

    let fullPalette = $nuxt.context.$mooncat.lib.fullPalette(catId)
    return fullPalette.slice(114)
  },
}

export const mutations = {
  libLoaded(state, bool) {
    state.libLoaded = bool
    mcbLog.info(`Library loaded`)
  },
}

export const actions = {
  libLoaded({ commit }, payload) {
    return new Promise((resolve, reject) => {
      commit('libLoaded', payload.loaded)
      resolve()
    })
  },
}
